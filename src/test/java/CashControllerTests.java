import controllers.CashController;
import models.CoinType;
import org.junit.Test;

import java.util.Map;

import static models.CoinType.*;
import static models.CoinType.FIVE_CENTS;
import static models.CoinType.TEN_CENTS;
import static org.junit.Assert.assertEquals;

public class CashControllerTests {
    @Test
    public void refundingLogicAllCoinsTheSameTest() {
        CashController cashController = new CashController(CoinType.values());

        cashController.addCoins(TWO, 10);
        cashController.addCoins(ONE, 5);
        cashController.addCoins(TEN_CENTS, 50);

        Map<CoinType, Integer> coins = cashController.getRefundCoins(10);
        assertEquals(coins.get(TWO).intValue(), 5);
        assertEquals(coins.keySet().size(), 1);
        assertEquals(cashController.getCoinAmountByType()[TWO.ordinal()], 5);
        assertEquals(cashController.getCoinAmountByType()[ONE.ordinal()], 5);
        assertEquals(cashController.getCoinAmountByType()[TEN_CENTS.ordinal()], 50);
        assertEquals(cashController.getTotalMoneyAmount(), 20, 0.001);
    }

    @Test
    public void refundingLogicPartialCoinTypeAmountTest() {
        CashController cashController = new CashController(CoinType.values());

        cashController.addCoins(TWO, 10);
        cashController.addCoins(ONE, 5);
        cashController.addCoins(TEN_CENTS, 50);

        Map<CoinType, Integer> coins = cashController.getRefundCoins(19);
        assertEquals(coins.get(TWO).intValue(), 9);
        assertEquals(coins.get(ONE).intValue(), 1);
        assertEquals(coins.keySet().size(), 2);
        assertEquals(cashController.getCoinAmountByType()[TWO.ordinal()], 1);
        assertEquals(cashController.getCoinAmountByType()[ONE.ordinal()], 4);
        assertEquals(cashController.getCoinAmountByType()[TEN_CENTS.ordinal()], 50);
        assertEquals(cashController.getTotalMoneyAmount(), 11, 0.001);
    }

    @Test
    public void refundingLogicSkippingCoinTypesTest() {
        CashController cashController = new CashController(CoinType.values());

        cashController.addCoins(TWO, 10);
        cashController.addCoins(ONE, 5);
        cashController.addCoins(TEN_CENTS, 50);
        cashController.addCoins(FIVE_CENTS, 50);


        Map<CoinType, Integer> coins = cashController.getRefundCoins(4.5);
        assertEquals(coins.get(TWO).intValue(), 2);
        assertEquals(coins.get(TEN_CENTS).intValue(), 5);
        assertEquals(coins.keySet().size(), 2);
        assertEquals(cashController.getCoinAmountByType()[TWO.ordinal()], 8);
        assertEquals(cashController.getCoinAmountByType()[ONE.ordinal()], 5);
        assertEquals(cashController.getCoinAmountByType()[TEN_CENTS.ordinal()], 45);
        assertEquals(cashController.getCoinAmountByType()[FIVE_CENTS.ordinal()], 50);
        assertEquals(cashController.getTotalMoneyAmount(), 28, 0.001);
    }

    @Test
    public void refundingLogicUsingAllAmountTest() {
        CashController cashController = new CashController(CoinType.values());

        cashController.addCoins(TWO, 10);
        cashController.addCoins(ONE, 5);
        cashController.addCoins(TEN_CENTS, 50);
        cashController.addCoins(FIVE_CENTS, 50);


        Map<CoinType, Integer> coins = cashController.getRefundCoins(27.55);
        assertEquals(coins.get(TWO).intValue(), 10);
        assertEquals(coins.get(ONE).intValue(), 5);
        assertEquals(coins.get(TEN_CENTS).intValue(), 25);
        assertEquals(coins.get(FIVE_CENTS).intValue(), 1);
        assertEquals(coins.keySet().size(), 4);
        assertEquals(cashController.getCoinAmountByType()[TWO.ordinal()], 0);
        assertEquals(cashController.getCoinAmountByType()[ONE.ordinal()], 0);
        assertEquals(cashController.getCoinAmountByType()[TEN_CENTS.ordinal()], 25);
        assertEquals(cashController.getCoinAmountByType()[FIVE_CENTS.ordinal()], 49);
        assertEquals(cashController.getTotalMoneyAmount(), 4.95, 0.001);
    }
}
