import controllers.CashController;
import controllers.ProductsController;
import controllers.VendingMachineController;
import models.ClientDelivery;
import models.CoinType;
import models.Product;
import org.junit.Test;

import java.util.UUID;

import static models.CoinType.*;
import static org.junit.Assert.assertEquals;

public class VendingMachineTests {
    private Product coke = new Product(UUID.randomUUID(), "Coke", 1.5);
    private Product sprite = new Product(UUID.randomUUID(), "Sprite", 1.45);
    private Product water = new Product(UUID.randomUUID(), "Water", 0.9);

    private ProductsController productsController = new ProductsController(coke, sprite, water);
    private CashController cashController = new CashController(CoinType.values());
    private VendingMachineController vendingMachineController = new VendingMachineController(cashController, productsController);

    @Test
    public void userAsksForProductOnEmptyMachineTest() {
        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(FIVE_CENTS);

        ClientDelivery deliver = vendingMachineController.deliverProduct(water);

        assert(deliver == null);

        resetMachine();
    }

    @Test
    public void userAsksForAvailableProductWithExactMoneyTest() {
        vendingMachineController.refillProduct(sprite, 5);

        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(FIVE_CENTS);

        ClientDelivery deliver = vendingMachineController.deliverProduct(sprite);

        assertEquals(deliver.getProduct(), sprite);
        assertEquals(deliver.getRefund().keySet().size(), 0);

        resetMachine();
    }

    @Test
    public void userAsksForAvailableProductWithRefundTest() {
        vendingMachineController.refillProduct(sprite, 5);

        vendingMachineController.refillCoin(TWO, 20);
        vendingMachineController.refillCoin(ONE, 20);
        vendingMachineController.refillCoin(FIFTY_CENTS, 100);
        vendingMachineController.refillCoin(TEN_CENTS, 100);

        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(TEN_CENTS);
        vendingMachineController.insertCoin(FIVE_CENTS);

        ClientDelivery deliver = vendingMachineController.deliverProduct(sprite);

        assertEquals(deliver.getProduct(), sprite);
        assertEquals(deliver.getRefund().get(TWO).intValue(), 1);
        assertEquals(deliver.getRefund().get(ONE).intValue(), 1);
        assertEquals(deliver.getRefund().get(TEN_CENTS).intValue(), 1);
        assertEquals(deliver.getRefund().keySet().size(), 3);

        resetMachine();
    }


    @Test
    public void cancelRequestMoneyAmountTest() {
        vendingMachineController.insertCoin(ONE);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(TWENTY_CENTS);
        vendingMachineController.insertCoin(FIVE_CENTS);

        assertEquals(vendingMachineController.getClientCumulatedMoneyAmount(), 1.45, 0.0001);
        ClientDelivery deliver = vendingMachineController.cancelRequest();
        assertEquals(vendingMachineController.getClientCumulatedMoneyAmount(), 0, 0.0001);
        assertEquals(deliver.getRefund().get(ONE).intValue(), 1);
        assertEquals(deliver.getRefund().get(TWENTY_CENTS).intValue(), 2);
        assertEquals(deliver.getRefund().get(FIVE_CENTS).intValue(), 1);
        assertEquals(deliver.getRefund().keySet().size(), 3);

        resetMachine();
    }

    private void resetMachine() {
        productsController = new ProductsController(coke, sprite, water);
        cashController = new CashController(CoinType.values());
        vendingMachineController = new VendingMachineController(cashController, productsController);
    }
}