import controllers.ProductsController;
import models.Product;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class ProductControllerTests {
    private Product coke = new Product(UUID.randomUUID(), "Coke", 1.5);
    private Product sprite = new Product(UUID.randomUUID(), "Sprite", 1.45);
    private Product water = new Product(UUID.randomUUID(), "Water", 0.9);
    private Product craftBeer = new Product(UUID.randomUUID(), "CraftBeer", 10.0);

    private ProductsController productsController = new ProductsController(coke, sprite, water);

    @Test
    public void ProductRefillingTest() {
        productsController.incrementProductBy(coke, 10);
        productsController.incrementProductBy(sprite, 5);
        productsController.incrementProductBy(water, 20);
        productsController.incrementProductBy(craftBeer, 1);

        assertEquals(productsController.getProducts().get(coke).intValue(), 10);
        assertEquals(productsController.getProducts().get(sprite).intValue(), 5);
        assertEquals(productsController.getProducts().get(water).intValue(), 20);
        assertEquals(productsController.getProducts().get(craftBeer).intValue(), 1);


        productsController.incrementProductBy(coke, 2);
        productsController.incrementProductBy(sprite, 2);
        productsController.incrementProductBy(water, 2);

        assertEquals(productsController.getProducts().get(coke).intValue(), 12);
        assertEquals(productsController.getProducts().get(sprite).intValue(), 7);
        assertEquals(productsController.getProducts().get(water).intValue(), 22);

        productsController.decrementProductBy(coke, 40);
        assertEquals(productsController.getProducts().get(coke).intValue(), 0);
    }
}
