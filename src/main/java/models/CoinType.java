package models;

public enum CoinType {
    FIVE_CENTS (0.05), TEN_CENTS (0.1), TWENTY_CENTS (0.2), FIFTY_CENTS (0.5), ONE (1), TWO (2);

    private double value;
    public double getValue() {
        return value;
    }

    public int getIndex() {
        return this.ordinal();
    }

    private CoinType(double value) {
        this.value = value;
    }
}