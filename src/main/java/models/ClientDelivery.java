package models;

import java.util.Map;

public class ClientDelivery {
    private Product product;
    private Map<CoinType, Integer> refund;

    public ClientDelivery(Product product, Map<CoinType, Integer> refund) {
        this.product = product;
        this.refund = refund;
    }

    public Product getProduct() {
        return product;
    }

    public Map<CoinType, Integer> getRefund() {
        return refund;
    }
}
