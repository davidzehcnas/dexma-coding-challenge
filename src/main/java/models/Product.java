package models;

import java.util.Objects;
import java.util.UUID;


public class Product {
    private UUID id;
    private String name;
    private Double price;

    public Product(UUID id, String name, Double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof Product)) return false;

        Product anotherProduct = (Product) object;
        return id.equals(anotherProduct.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}
