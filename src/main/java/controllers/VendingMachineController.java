package controllers;

import models.ClientDelivery;
import models.CoinType;
import models.Product;

import java.util.Map;

public class VendingMachineController {
    private CashController cashController;
    private ProductsController productsController;
    private double clientCumulatedMoneyAmount;

    public VendingMachineController(CashController cashController, ProductsController productsController) {
        this.cashController = cashController;
        this.productsController = productsController;
        clientCumulatedMoneyAmount = 0;
    }

    public ClientDelivery deliverProduct(Product desiredProduct) {
        if (isProductDeliverable(desiredProduct)) {
            productsController.decrementProductBy(desiredProduct, 1);
            Map<CoinType, Integer> refundCoins = cashController.getRefundCoins(clientCumulatedMoneyAmount - desiredProduct.getPrice());
            clientCumulatedMoneyAmount = 0;
            return new ClientDelivery(desiredProduct, refundCoins);
        } else {
            return null;
        }
    }

    public ClientDelivery cancelRequest() {
        Map<CoinType, Integer> refundCoins = cashController.getRefundCoins(clientCumulatedMoneyAmount);
        clientCumulatedMoneyAmount = 0;

        return new ClientDelivery(null, refundCoins);
    }

    public void insertCoin(CoinType coinType) {
        cashController.addCoin(coinType);
        clientCumulatedMoneyAmount += coinType.getValue();
    }

    public void refillProduct(Product product, int amount) {
        productsController.incrementProductBy(product, amount);
    }

    public void refillCoin(CoinType coinType, int amount) {
        cashController.addCoins(coinType, amount);
    }

    private boolean isProductDeliverable(Product desiredProduct) {
        return productsController.isProductAvailable(desiredProduct) && desiredProduct.getPrice() <= clientCumulatedMoneyAmount;
    }

    public double getClientCumulatedMoneyAmount() {
        return clientCumulatedMoneyAmount;
    }
}
