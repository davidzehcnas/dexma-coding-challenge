package controllers;

import models.Product;

import java.util.HashMap;

public class ProductsController {
    private HashMap<Product, Integer> products;

    public ProductsController(Product... products) {
        this.products = new HashMap<>();

        for (Product product : products)
            incrementProductBy(product, 0);
    }

    public boolean isProductAvailable(Product product) {
        return products.containsKey(product) && products.get(product) > 0;
    }

    public void incrementProductBy(Product product, int increment) {
        if (products.computeIfPresent(product, (key, oldValue) -> oldValue + increment) == null)
            products.put(product, increment);
    }

    public void decrementProductBy(Product product, int decrement) {
        products.computeIfPresent(product, (key, oldValue) -> Math.max(0, oldValue - decrement));
    }

    public HashMap<Product, Integer> getProducts() {
        return products;
    }
}
