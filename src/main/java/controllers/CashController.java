package controllers;

import com.google.common.collect.Lists;
import models.CoinType;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class CashController {
    private int[] coinAmountByType;
    private CoinType[] coinTypes;
    private CoinType[] reversedCoinTypes;

    public CashController(CoinType[] coinTypes) {
        coinAmountByType = new int[coinTypes.length];
        Arrays.fill(coinAmountByType, 0);

        this.coinTypes = coinTypes;
        reversedCoinTypes = Lists.reverse(Arrays.asList(coinTypes)).toArray(new CoinType[coinTypes.length]);
    }

    public void addCoins(CoinType coinType, int amount) {
        this.coinAmountByType[coinType.getIndex()] += amount;
    }

    public void addCoin(CoinType coinType) {
        addCoins(coinType, 1);
    }

    public double getTotalMoneyAmount() {
        double total = 0.0;
        for (CoinType coinType : coinTypes) {
            total += coinAmountByType[coinType.getIndex()] * coinType.getValue();
        }
        return total;
    }

    public Map<CoinType, Integer> getRefundCoins(double moneyAmount) {
        if (moneyAmount <= 0) return new Hashtable<>();
        else {
            BigDecimal pendingAmount = toBigDecimal(moneyAmount);
            Map<CoinType, Integer> refundedCoins = new Hashtable<>();

            for (CoinType coinType : reversedCoinTypes) {
                boolean coinValueLowerThanPending = toBigDecimal(coinType.getValue()).compareTo(pendingAmount) <= 0;
                if (coinAmountByType[coinType.getIndex()] > 0 && coinValueLowerThanPending) {
                    int numCoins = Math.min(coinAmountByType[coinType.getIndex()], (int) (pendingAmount.doubleValue() / coinType.getValue()));
                    refundedCoins.put(coinType, numCoins);
                    pendingAmount = pendingAmount.subtract(toBigDecimal(numCoins * coinType.getValue()));
                    coinAmountByType[coinType.getIndex()] -= numCoins;
                }
            }

            return pendingAmount.compareTo(toBigDecimal(0)) == 0 ? refundedCoins : new Hashtable<>();
        }
    }


    public int[] getCoinAmountByType() {
        return coinAmountByType;
    }

    private BigDecimal toBigDecimal(double value) {
        BigDecimal bigDecimal = new BigDecimal(value, MathContext.DECIMAL32);
        return bigDecimal.setScale(2, RoundingMode.HALF_EVEN);
    }
}